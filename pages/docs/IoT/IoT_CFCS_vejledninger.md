# Center for cybersikkerhed's udgivelser omkring sikker brug af IoT enheder

Center for cybersikkerhed (CFCS) har vurderet at truslen for cyberangreb mod IoT enheder er MEGET HØJ. CFCS udkom derfor d. 3. november 2023 med en række udgivelser med fokus på cybertruslen mod IoT enheder, samt vejledninger til hvordan man bedst kan beskytte dem.

Udgivelserne består af tre dele:

* En trusselsvurdering: *”Cybertruslen mod IoT-enheder”*

* En vejledning: *”Vejledning: Beskyt IoT-enheder"*

* En kort guide til hvordan organisationer kan komme i gang med at beskytte sine IoT-enheder: *”Kom i gang med at beskytte IoT”*

### **CFCS trusselsvurdering: *"Cybertruslen mod IoT enehder"***

CFCS udgivelse "Cybertruslen mod IoT enheder" er en trusslesvurdering for IoT enheder.

I trusselsvurderingen vurder CFCS af truslen for cyberangreb mod IoT enheder er MEGET HØJ.

CFCS vurderer at hackere sandsynligvis vil forsætte på lang sigt med at forsøge at lave cyberangreb mod både danske og udenlandske IoT enheder.

CFCS vurderer desuden, at det er sandsynligt at hackere anser IoT enheder som et attraktivt mål, da de ofte er mere eksponeret og dårligere beskyttet end almindelige computere, og derfor er nemmere at kompromittere. Kompromittering af IoT enheder vurderes af CFCS til typisk at ske via udnyttelse af sårbarheder eller brute force angreb.

Derudover vurderer CFCS, at IoT enheder ofte kompromitteres med det formål at bruge IoT enhederne som bots i botnets eller bruger dem som en indledende angrebsvinkel for andre angreb mod en organisation.

Ved kompromittering af IoT enheder med formålet at bruge dem som bots i botnets vurdere CFCS, at det er sandsynligt, at hackere vil gøre dette for at udnytte enhedens maskinkraft - typisk til DDoS angreb eller generering af kryptovaluta.
I dette tilfælde sker kompromitteringen ikke for at ramme den organisation som ejer IoT enheden, men for at udnytte IoT enhedens maskinkraft.

CFCS vurder dog, at det også er sandsynligt, at hackere vil kompromittere IoT enheder for at bruge dem som den indledende angrebsvinkel, for derefter at få adgang længere ind i organisationens system. Her angribes IoT enheden ikke fordi den i sig selv er interessant, men fordi den kan give hackerne adgang ind på det netværk, som den er forbundet til. CFCS vurderer, at når hackerne har fået adgang længere ind i netværket, er det sandsynligt, at de vil udnytte det til ransomware angreb samt evt. stjæle sensitive data med henblik på afpresning.

De kan desuden blive brugt til cyberspionage – blandt andet hvis de er lavet i lande udenfor Danmarks sikkerhedspolitiske kreds, kan fremmede stater anvende dem til cyberspionage. 

Det er ifølge CFCS meget sandsynligt, at statsstøttede hackere kompromitterer IoT enheder med det formål, at få adgang til det netværk IoT enheden er koblet op på, for derefter at udfører cyberspionage mod det pågældende netværk.

### **CFCS guide: *"Kom i gang med at beskytte IoT"***

CFCS har lavet en kort guide til hvordan en organisation kan komme bedst muligt i gang med at beskytte sine IoT enheder.

Guiden er inddelt i to dele:

*	Før anskaffelse af en IoT enhed

*	Efter anskaffelse af en IoT enhed

**Før anskaffelse af en IoT enhed**

*	Tag stilling til om IoT enheden skal kobles på internettet. Er det ikke nødvendigt så lad være – det gør brugen af den mere sikker

* Undersøg og beslut hvilken leverandør af IoT enheder man vil bruge. Find ud af hvilken data leverandøren indsamler, hvordan leverandøren anvender disse data og tag stilling til om man vil dele disse data med leverandøren


*	Ved køb af IoT bør den ifølge CFCS leve op til følgende tekniske krav:

    * Der skal være løbende opdateringer af IoT enheden

    * Det skal være muligt at ændre standardlogin på IoT enheden


**Efter anskaffelse af en IoT enhed**

*	Ændre standardloginoplysningerne og lav password på minimum 15 tegn

*	Slå funktioner fra som man ikke skal bruge

*	Undgå at koble IoT enheden på det ”almindelige netværk” – hav i stedet et særskilt subnet til IoT enheder med beskyttelse af firewall.

*	Hold løbende IoT enheden opdateret 

*	Lav en oversigt over hvilke IoT enheder man har – så man hele tiden har overblik over dem

*	Skil sig af med gamle enheder som ikke længere bliver opdateret fra leverandøren

### **CFCS vejledning: *”Vejledning: Beskyt IoT-enheder"***

CFCS har lavet en vejledning med deres bud på best pratice til beskyttelse af IoT enheder.

**Politik for IoT enheder**

Det første punkt i CFCS vejledning omhandler, at organisationen bør have en politik for IoT sikkerhed. Det er ifølge CFCS it-ledelsens ansvar at have en forståelse for trusler og sårbarheder ved IoT enheder, vurdere hvad der er et acceptabelt risikoniveau og på det grundlag udarbejde en intern politik for brugen af IoT enheder i organisationen.

IoT politikken skal sørge for, at alle medarbejdere kender deres rolle og ansvarsområde indenfor IoT også i en beredskabssituation, hvor IoT enheder er involveret.

Politikken bør desuden indeholde organisationens krav til, hvilke data IoT enhederne må opsamle, krav til data som bevæger sig til og fra IoT enheder samt regler for hvem der må tilgå data opsamlet af IoT enheder.

I IoT politikken skal også indeholde organisationens krav til IoT leverandører. Oanisationen skal her vurdere, hvilke andre lande deres data må blive behandlet i, da nogle lande har logivning, som giver myndighederne i landet ret til at få udleveret data fra virksomheder i det pågældende land.

Organisationen skal have styr på, om der gælder særlig lovgivning indenfor deres forretningsområde, som har betydning for de sikkerhedskrav, organisationen skal stille til IoT enheder.

IoT politikken bør evalueres og revurderes af ledelsen mindst en gang om året, eller hvis der sker væsentlige ændringer.

**Krav til leverandører**

Ifølge CFCS bør organisationen stille sikkerhedskrav til leverandør af IoT enheder og -systemer, for at sikre at leverandøren opfylder organisationens sikkerhedskrav. 

CFCS anbefaler, at organisationer vælger en IoT leverandør, som kan dokumentere en række oplysninger, heriblandt hvor længe IoT enheden modtager sikkerhedsopdateringer, leverandørens politik for sikkerhedstests og hvilke data leverandøren indsamler. Organisationen kan derved vælge en leverandør, som lever op til organisationens sikkerhedskrav. 

Organisationen bør sikre sig, at leverandøren sikkerhedstester og opdateres deres enheder, når de bliver bekendt med sårbarheder.

**Hærdning af IoT enheder**

CFCS anbefaler, at man lader være med at koble enheden op på internettet, hvis dette ikke er nødvendigt. CFCS anbefaler desuden, at alle unødvendige interfaces, protokoller, funktioner, hardware og software slås fra – også ved enheder der er offline.

**Adgangskontrol**

Organisationen bør ifølge CFCS have adgangskontrol til IoT enheder. Organisationer skal undgå svage password. CFCS anbefaler passwords på minimum 15 tegn, og gerne med både tal, bogstaver, grafiske tegn og speciale tegn.

Derudover anbefaler CFCs flerfaktor autentifikation, når det er muligt.

CFCS anbefaler, at organisationer ikke anskaffer sig IoT enheder med prædefineret loginoplysninger, som ikke kan ændres, da hackere så kan slå loginoplysningerne op på nettet og forsøge sig med standard loginet.

Organisationer bør også overveje den fysiske adgang til IoT enheden, så kun relevante personer kan tilgå display og interaktionsmekanismer. Beskyttelse af den fysiske adgang kan ske ved fysisk at afskærme IoT enheden eller ved password beskyttelse.

**Netværkssegmentering**

Ifølge CFCS bør organisationen segmentere IoT enheder fra resten af netværket. Ved at segmentere netværket mindskes sandsynligheden for, at et eventuelt angreb kan sprede sig

Ved at segmentere IoT enheder fra resten af organisationens netværk og konfigurer firewalls kan organisationen begrænse muligheden for, at malware spreder sig i netværket, eller at uvedkommende får adgang til sensitive data eller forretningskritiske systemer.

**Processer for opdatering og behandling af sårbarheder på IoT enheder**

Organisationer bør ifølge CFCS sikre, at deres IoT enheder er opdateret til nyeste version. Især sikkerhedsopdateringer er vigtige, da de lukke opdaget sårbarheder.

CFCS anbefaler at slå automatisk opdatering til for sikkerhedsopdateringer, når det er muligt.

Organisationen skal have nogle retningslinjer for, hvornår en opdatering senest skal installeres, og hvad organisationen skal gøre, hvis ikke en opdatering til en kendt sårbarhed kommer hurtigt nok. Det kan f.eks. være at afkoble enheden fra internettet indtil sikkerhedsopdateringen kommer eller ved at fjerne eller udskifte IoT enheden.

**Monitorering og logning**

Organisationen bør ifølge CFCS logge tilstande, hændelser og netværkstrafik fra IoT enheder, da det gør organisationen i stand til at kunne opdage, følge og analysere anomalier og hændelser på IoT enheder og derved opdage mulige sikkerhedshændelser.

Logs kan også hjælpe med at afdække, hvor hackere kom ind i organisationens it-infrastruktur, hvilket kan gøre det muligt for organisationen at udbedre sine sårbarheder. Organisationen kan også se, om hackerne har bevæget sit rundt i organisationens øvrige infrastruktur, og hvilke handlinger der er foretaget. 

Ifølge CFCS bør logs opbevares i mindst 13 måneder.

CFCS anbefaler også at monitorer anomal aktivitet. Altså alt aktivitet som afviger fra normalbilledet. 

**Kyrpteret kommunikation**

Organisationer bør kryptere IoT enheders kommunikation både i trafikken til og fra enhederne, men også for alle de andre services som man kan tilgå enhederne igennem.

Hver IoT enheder bør have sin egen unikke krypterings nøgle, så hvis hackere får adgang til en, har de ikke også adgang til resten.

De kryptografiske algoritmer og nøgler skal kunne udskiftes i takt med, at algoritmerne med tiden bliver forældede. 

**Processor for behandling og sletning af data**

Organisationer bør ifølge CFCS sikre, at behandling af data på IoT enheder og systemer sker på forsvarlig vis, og at det er let at slette data.

CFCS anbefaler, at organisationen kan redegøre for, hvem der behandler hvilke data på organisationens IoT enheder, og på hvilken måde det foregår. Samt sikre sig at der er indhentet de fornødne tilladelser til behandling af data på organisationens IoT enheder og -systemer. 

**Processor for bortskaffelse og genbrug**

Når en IoT enhed ikke længere benyttes bør organisationen tage stilling til, om enheden skal destrueres eller genbruges udenfor organisationen. Organisationen bør her tage stilling til, hvilken data enheden har behandlet, og sikre at bortskaffelse først sker efter sikker sletning af enhedens data. Så dataene ikke senere kan genskabes af andre.

CFCS anbefaler, at hvis IoT enheden har opbevaret ikke sensitiv data, kan IoT enheden genbruges eller bortskaffes efter at være blevet nulstillet eller renset for al data. Har IoT enheden derimod opbevaret sensitive data, skal den destrueres. 

### **Link til Center for Cybersikkerheds udgivelser omkring beskyttelse af IoT enheder fra d. 3. november 2023**

[https://www.cfcs.dk/da/nyheder/2023/internet-of-things/](https://www.cfcs.dk/da/nyheder/2023/internet-of-things/ "Link til CFCS udgivelser om IoT")

