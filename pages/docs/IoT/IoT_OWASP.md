---
hide:
  - footer
---

# OWASP "Internet of Things Top 10"

OWASP har en liste kaldet "Internet of Things Top 10", som er en liste over, hvad OWASP mener er de 10 største sårbarheder ved IoT enheder på nuværende tidspunkt.

OWASP opdatere løbende deres lister efterhånden, som nye sårbarheder kommer til, eller allerede kendte sårbarheder begynder at udgøre en større trussel end nogen af de sårbarheder, som allerede er på listen.

Den seneste udgave af OWASP "Internet of Things Top 10" er fra 2018 og kan ses herunder:

![OWASP IoT Top 10](../images/OWASP-IoT-Top-10-2018.jpg)

OWASP's "Internet of Things Top 10" er et godt sted at starte, når man vil forsøge at sikre sine IoT enheder imod at blive kompromitteret.

OWASP Top 10 er dog ikke en udtømmende liste. Der findes andre store sårbarheder, men dette er dem, som OWASP mener, er de mest kritiske på nuværende tidspunkt.

Det kan derfor være et godt sted at starte at sikre sine IoT enheder mod disse sårbarheder.
