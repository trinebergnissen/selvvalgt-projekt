---
hide:
  - footer
---

# Hvad er en IoT enhed

![Alt text](../images/iot.webp)

**Ifølge Dansk Industri er en IoT enhed:** *”et sammenkoblet netværk af fysiske objekter, altså ting. Objekterne er integreret med sensorer, software og andre teknologier med det simple formål, at forbinde og udveksle data med andre enheder og systemer over internettet”.*

**Ifølge Sikker DIgital er en IoT enhed:** *"teknologier, der sammenkobler produkter og systemer via internettet, så produkterne er i stand til at sende og modtage data*


**Ifølge Center for cybersikkerhed er en IoT enhed:** *”IoT er en samlebetegnelse for alle enheder, der forbindes til internettet med henblik på bl.a. fjernstyring. Eksempelvis et kamera, der kan identificere biler ud fra nummerplader og tage betaling for parkering eller en printer, som hurtigt ordnes af en it-leverandør via fjernadgang. Begrebet dækker i denne vejledning ikke over almindelige computere, servere eller telefoner samt operationelle teknologier som f.eks. industrielle kontrolsystemer”*

Kort sagt kan man altså sige, at en IoT enhed er en enhed, som måler noget på den fysiske verden, og som samtidig er koblet op på internettet, og er i stand til at sende og modtage data over internettet.

**Eksempler på IoT enheder:**

* Overvågningskameraer

* Termostater

* Varmesensorer

* Smart køleskabe

* Hjemme assistenter - så som Google Home og Alexa

 * Smart cars

 Og mange andre.