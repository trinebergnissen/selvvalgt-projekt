# ISO 27000 standarder

ISO har nogle standarder i ISO 27000 serien som henvender sig specifikt til IoT enheder:

* ISO 27400 – Cybersecurity – IoT security and privacy – Guidelines

* ISO 27402 – Cybersecurity – IoT security and privacy – Device baseline requirements

Udover standarderne i ISO 27000 serien, har ISO også en række andre standarder indenfor IoT, herunder:

* ISO 30141 – Internet of Things (IoT) – Reference architecture

* ISO 30149 ED1 – Internet of Things (IoT) – Trustworthiness Principles

* ISO 30161-1 ED1 – Internet of Things (IoT) – Data exchange platform for IoT services - Part 1: General requirements and architecture

* ISO 30162 – Internet of Things (IoT) – Compatibility requirements and model for devices within industrial IoT systems

