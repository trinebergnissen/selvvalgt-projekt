---
hide:
  - footer
---

# IoT sårbarheder

### Typiske sårbarheder ved IoT enheder

IoT enheder har nogle typiske sårbarheder, som ofte går igen i forskellige typer af IoT enheder.

**Svag autentifikation**

En af de typiske sårbarheder som ofte ses ved IoT enheder er brugen af svage eller default passwords, hvilket kan udnyttes af en trusselsaktør til at få adgang til IoT enheden.

Derudover gør IoT enheder ofte ikke brug af 2 faktor autentifikation.

**Offentlig tilgængelige**

En anden sårbarhed som går igen for mange IoT enheder er, at de hænger offentligt tilgængeligt, hvilke gør at en trusselsaktør nemmere kan få fysisk adgang til IoT enheden. Det betyder, at trusselsaktøren kan stjæle IoT enheden og sidde derhjemme i rog og mag og forsøge at få adgang til IoT enheden uden at blive forstyrret.

**Manglende opdatering**

Mange IoT enheder er også sårbare, fordi de ikke løbende bliver opdateret med sikkerheds opdateringer, da de ofte blot er "once sold and done". Producenten bag IoT enhederne producere ofte blot IoT enheder og udgiver derefter ikke løbende opdateringer til IoT enhederne. Det betyder, at skulle der være en sårbarhed i IoT enheden, så bliver den aldrig rettet, men bliver ved med at være en sårbarhed, som kan angribes.

**Masseproduktion**

Derudover bliver IoT enheder ofte masse produceret. Det betyder, at hvis først en IoT enhed bliver opdaget med en sårbarhed, så vil andre lignende IoT enheder formegentlig have samme sårbarhed. Eller hvis en trusselsaktør får succes med et angreb mod en IoT enhed, så vil det samme angreb formegentlig også virke på andre IoT enheder, som er magen til.

**Manglende hardening**

En anden typisk sårbarhed ved IoT enheder er manglende hardening. Ved hardening mindskes angrebsfalderne. Det kan f.eks. ske ved at default passwords ændres, unødvendigt software fjernes, unødvendige porte deaktiveres osv. Ved manglende hardening er angrebsfladerne derfor ikke blevet mindsket til det kun absolut nødvendige, og der findes derfor flere angrebsfalder, som trusselsaktører kan angribe.

**Manglende kryptering**

Mange IoT enheder opbevarer og sender data som ikke er krypteret. Det betyder, at hvis en trusselsaktør får adgang ind i IoT enheden eller får adgang til de sendte data, kan dataene aflæses.

**Manglende host baseret firewall**

Mange IoT enheder har ikke en host baseret firewall, som kan være med til at beskytte enheden. 

**OWASP Top 10**

Udover de ovenfor nævne sårbarheder har OWASP lavet en liste med Top 10 sårbarheder ved IoT enheder. Nogle af sårbarhederne ovenfor går igen i OWASPs Top 10. De resterende sårbarheder på OWASPs Top 10, kan ses under menupunktet "OWASP - Internet of Things Top 10".

### Hvorfor udgør usikre IoT enheder en trussel
Der findes flere årsager til at usikre IoT enheder udgør en trussel for organisationer.

Nogle IoT enheder indeholder sensitive data, og de udgør derved i sig selv en trussel for organisationen, hvis de bliver kompromitteret.

IoT enheder som ikke indeholder sensitive data, og derved ikke i sig selv udgør en trussel for organisationen, kan dog alligevel udgøre en trussel. Dette skyldes, at IoT enheder kan udnyttes som angrebsflade videre ind i organisationens netværk.
Trusselsaktøren kan derved få fat i sensitive data, eller angribe videre ind i organisationens systemer og sprede malware f.eks. i form af ransomware.

Derudover kan trusselsaktører som får kompromitteret en usikker IoT enhed udnytte IoT enheden som en bot i et botnet og f.eks. anvende IoT enheden til DDoS angreb mod andre organisationer.
