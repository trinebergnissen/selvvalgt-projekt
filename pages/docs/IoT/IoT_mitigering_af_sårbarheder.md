# IoT - forslag til mitigering af sårbarheder

Da IoT enheder ofte har en række sårbarheder, findes der forskellige foranstaltninger, som kan implementeres for at mitigere sårbarhederne. Herunder ses en liste med forslag til foranstaltninger til mitigering af nogle af de mest typiske sårbarheder:

* Brug stærk autentifikation

    * Brug aldrig svage eller default passwords

    * Brug 2 faktor authentication hvor det er muligt

* Deaktiver unødvendige funktioner

    * Derved mindskes antallet af angrebsflader

* Hvis der kommer opdateringer til IoT enhederne, så sørg for at enhederne er opdateret - særligt hvis der er tale om sikkerhedsopdateringer

* Implementer fysiske sikkerhedsforanstaltninger så IoT enhederne ikke fysisk kan tilgås af andre

    * Det sikre både imod at en trusselsaktør kan tilgå displayet på IoT enheden, sætte et USB-stik i IoT enheden eller stjæle IoT enheden med hjem, hvor han/hun har fred og ro til at forsøge at kompromittere enheden

* Isoler IoT enhederne fra resten af netværket via netværkssegemntering 

    * Hav et subnet udelukkende til IoT enheder. På den måde spredes malware "kun" til andre IoT enheder og ikke til resten af organisationens enheder, hvis en IoT enhed bliver kompromitteret og inficeret med malware

    * Derudover beskytter netværkssegmentering også imod at trusselsaktører får adgang længere ind i organisationens netværk og systemer, hvis det lykkes dem af kompromittere end IoT enhed, da man så har begrænse deres adgang til det enkelte subnet i stedet for til hele organisationens netværk

* Beskyt IoT enhederne bag firewalls

    * Da mange IoT enheder har en minimal processorkraft, har de svært ved at beskytte sig selv/det er svært at implementere beskyttesle i dem. Det kan derfor være med til at mitigere IoT enhedernes sårbarheder at beskytte dem bag netværks baseret firewalls

* Brug kryptering til at beskytte dataene 

    * Både når de opbevares på IoT enheden og når de sendes frem og tilbage mellem IoT enheden og andre af organisationens enheder

Disse var blot forslag til foranstaltninger til mitigering af nogle af de mest typiske sårbarheder. Dog har Center for Cybersikkerhed lavet en vejledning med deres bud på best practice til beskyttelse af IoT enheder. Denne vejledning er beskrevet nærmere under punktet "CFCS udgivelser omkring sikker brug af IoT enheder".