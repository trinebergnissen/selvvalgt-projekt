---
hide:
  - footer
  - toc
---

# IoT enheder

![Alt text](images/IoT.jpg)

Følgende projekt beskæftiger sig med IoT enheder.

Projektet beskæftiger sig først med definitonen af hvad en IoT enhed er. Dernæst beskæftiger det sig med de typiske sårbarheder, som ofte ses ved IoT enheder, samt foranstaltninger som kan benyttes til at mitigere IoT enheders sårbarheder. Til slut beskæftiger projektet sig med standarder og vejledninger inden for beskyttelse af IoT enheder herunder OWASP Top 10, ISO standarder og Center for Cybersikkerheds udgivelser omkring sikker brug af IoT enheder.

Under menupunktet projektplan findes projektplanen for projekt.

Under menupunktet IoT findes emnerne:

* Hvad er en IoT enhed

* IoT sårbarheder

* IoT - mitigering af sårbarheder

* OWASP - Internet of Things Top 10

* ISO standarder indenfor IoT

* CFCS udgivelser omkring sikker brug af IoT enheder

Igennem projektet vil ondsindet personer, som via cyberangreb  forsøger at kompromittere organisationers systemer blive omtalt som trusselsaktører. Dog med udtagelse af siden "CFCS udgivelser omkring sikker brug af IoT enheder". Her benyttes ordet hacker, da det er det ord, som Center for Cybersikkerhed selv benytter i sine udgivelser.