---
Week: xx
Content: Introduktion til faget, data modellering
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Projektbeskrivelse for selvvalgt fordybelse

### **Emne:** IoT enheder

### **Beskrivelse af projektet**

Projektet omhandler grundlæggende viden om IoT enheder og de mest typiske sårbarheder. Der vil blive undersøgt, hvilke sikkerhedsforanstaltninger man kan implementere for at mitigere sårbarheder ved IoT enheder, samt hvilke standarder og vejledninger der findes inden for implementeringen af sikkerhed i IoT enheder.

### **Læringsmål for projektet**

**Viden**

Den studerende har viden om

* Hvad en IoT enheder er

* Typiske sårbarheder ved IoT enheder

* Sikkerhedsforanstaltninger imod angreb af en virksomheds IoT enheder

* Hvilke sikkerhedsmæssige overvejsler man skal gøre sig, hvis man vil implementere IoT enheder i sin virksomhed

* Standarder og vejledninger indenfor IoT enheder

**Færdigheder**

Den studerende kan

*	Identificere sårbarheder som IoT enheder kan have

*	Søge information om og navigere i standarder og vejledninger om IoT enheder

**Kompetencer**

Den studerende kan

* Håndtere analyser om hvilke sårbarheder der skal tages hånd om ved en IoT enhed



### **Milepæls plan for hele projektet**

**Inden første vejledning d. 19. september:**

* Hvad er IoT enhed

**Inden anden vejledning d. 10. oktober**

* Typiske sårbarheder ved IoT enheder

* Sikkerhedsforanstaltninger imod angreb af en virksomheds IoT enheder

* Identificere sårbarheder som IoT enheder kan have

**Inden tredje vejledning d. 31. oktober**

* Standarder og vejledninger indenfor IoT enheder

* Søge information og navigere i standarder og vejledninger om IoT enheder

* Håndtere analyser om hvilke sårbarheder der skal tages hånd om ved en IoT enhed

**Inden aflevering d. 14. november**

Fortsat

* Standarder og vejledninger indenfor IoT enheder

* Søge information og navigere i standarder og vejledninger om IoT enheder

* Håndtere analyser om hvilke sårbarheder der skal tages hånd om ved en IoT enhed


### **Link til GitLab projekt**

[https://trinebergnissen.gitlab.io/selvvalgt-projekt/](https://trinebergnissen.gitlab.io/selvvalgt-projekt/ "Link til gitlab page")
